\contentsline {section}{\numberline {1}Disclaimer}{2}
\contentsline {section}{\numberline {2}Executive Summary}{2}
\contentsline {section}{\numberline {3}Owner's Background}{2}
\contentsline {section}{\numberline {4}Customer Need and Pains}{3}
\contentsline {subsection}{\numberline {4.1}Research Problem}{3}
\contentsline {subsubsection}{\numberline {4.1.1}Integration and Portability}{3}
\contentsline {subsubsection}{\numberline {4.1.2}Information Presentation}{3}
\contentsline {subsubsection}{\numberline {4.1.3}Distraction and Safety}{4}
\contentsline {subsubsection}{\numberline {4.1.4}Routing Efficiency and Safety}{4}
\contentsline {section}{\numberline {5}Products and Services}{4}
\contentsline {subsection}{\numberline {5.1}Features}{4}
\contentsline {subsubsection}{\numberline {5.1.1}Integration and Portability}{4}
\contentsline {subsubsection}{\numberline {5.1.2}Information Presentation}{4}
\contentsline {subsubsection}{\numberline {5.1.3}Distraction and Safety}{5}
\contentsline {subsubsection}{\numberline {5.1.4}Routing Efficiency and Safety}{5}
\contentsline {subsubsection}{\numberline {5.1.5}Options}{5}
\contentsline {subsection}{\numberline {5.2}Limitations}{5}
\contentsline {subsection}{\numberline {5.3}Safety Regulations}{5}
\contentsline {subsubsection}{\numberline {5.3.1}Adaptation by the Market}{5}
\contentsline {section}{\numberline {6}The Market}{6}
\contentsline {subsection}{\numberline {6.1}Global Market Overview}{6}
\contentsline {subsection}{\numberline {6.2}Target Market Analysis}{6}
\contentsline {subsubsection}{\numberline {6.2.1}Standalone Device}{6}
\contentsline {subsubsection}{\numberline {6.2.2}In Vehicle System}{7}
\contentsline {subsection}{\numberline {6.3}Forecasted Sales}{8}
\contentsline {subsubsection}{\numberline {6.3.1}Standalone Product}{8}
\contentsline {subsubsection}{\numberline {6.3.2}In Vehicle System}{8}
\contentsline {section}{\numberline {7}Competitor Analysis}{9}
\contentsline {subsection}{\numberline {7.1}Intensity of Competitive Rivalry}{9}
\contentsline {subsection}{\numberline {7.2}Threat of New Substitute}{10}
\contentsline {subsection}{\numberline {7.3}Threat of New Entrants}{10}
\contentsline {subsection}{\numberline {7.4}Bargaining Power of Suppliers}{11}
\contentsline {subsection}{\numberline {7.5}Bargaining Power of Customers}{11}
\contentsline {section}{\numberline {8}Marketing Strategy}{11}
\contentsline {subsection}{\numberline {8.1}Overview}{11}
\contentsline {subsection}{\numberline {8.2}Advertisement}{11}
\contentsline {subsubsection}{\numberline {8.2.1}Online advertisement}{11}
\contentsline {subsubsection}{\numberline {8.2.2}Traditional Advertisement}{12}
\contentsline {subsubsection}{\numberline {8.2.3}Exhibitions \& Evaluation by Journalists}{12}
\contentsline {subsection}{\numberline {8.3}Distribution Network}{12}
\contentsline {subsubsection}{\numberline {8.3.1}Online Retailers}{12}
\contentsline {subsubsection}{\numberline {8.3.2}Physical Stores}{12}
\contentsline {section}{\numberline {9}Operations and Logistics}{13}
\contentsline {subsection}{\numberline {9.1}Manufacturing Strategy}{13}
\contentsline {subsection}{\numberline {9.2}Supply Chain}{13}
\contentsline {subsection}{\numberline {9.3}Lead Time}{13}
\contentsline {subsection}{\numberline {9.4}Product Life Cycle}{14}
\contentsline {subsection}{\numberline {9.5}Company Facilities}{14}
\contentsline {subsection}{\numberline {9.6}Customer Support and Warranty}{15}
\contentsline {section}{\numberline {10}Costs and Pricing}{15}
\contentsline {subsection}{\numberline {10.1}Costs}{15}
\contentsline {subsubsection}{\numberline {10.1.1}Product Capital Cost}{15}
\contentsline {subsubsection}{\numberline {10.1.2}Equipment and Office Cost}{16}
\contentsline {subsubsection}{\numberline {10.1.3}Marketing Cost}{16}
\contentsline {subsubsection}{\numberline {10.1.4}Salaries}{17}
\contentsline {subsection}{\numberline {10.2}Pricing}{17}
\contentsline {section}{\numberline {11}Legal Requirements}{17}
\contentsline {subsection}{\numberline {11.1}Registering Company}{17}
\contentsline {subsection}{\numberline {11.2}Registering Intellectual Property}{17}
\contentsline {section}{\numberline {12}Financial Forecast}{18}
\contentsline {subsection}{\numberline {12.1}Break Even Analysis}{18}
\contentsline {subsection}{\numberline {12.2}Investment and Returns}{18}
\contentsline {section}{\numberline {13}Backup Plan}{19}
\contentsline {subsection}{\numberline {13.1}Short Term}{19}
\contentsline {subsection}{\numberline {13.2}Long Term}{20}
